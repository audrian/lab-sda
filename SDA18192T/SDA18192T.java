import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/**
 * TP 2 SDA
 * Muhammad Audrian Ananda Priambodo
 * 1706043790 - CSUI 2017
 * with contributions by Ariq Naufal Satria
 */

class Permata {
   Integer color;
   Permata next;
   Permata prev;

   public Permata(Integer colorValue) {
       this.color = colorValue;
   }
}

class Kalung {
    Permata head;
    int size;
    int chainStartIdx;
    int chainEndIdx;
    int chainSize;
    int leftCount;
    int rightCount;

    public Kalung() {
        head = null;
    }

    Permata get(int index) {
        Permata temp = this.head;
        for (int i = 0; i < index; i++) {
            temp = temp.next;
        }
        return temp;
    }

    void insert(int color) {
        //Insert at the end
        Permata p = new Permata(color);
        if (head == null) {
            //Adapted from Geeks for Geeks
            p.next = p.prev = p;
            head = p;
        } else {
            Permata last = head.prev;
            p.prev = last;
            p.next = head;
            last.next = head.prev = p;
        }
        size++;
    }

    void insertFirst(int color) {
        Permata p = new Permata(color);
        if (head == null) {
            insert(color);
        } else {
            Permata last = head.prev;
            p.prev = last;
            p.next = head;
            last.next = head.prev = p;
            head = p;
            size++;
        }
    }

    void insertAt(int idx, int color) {
        //Insert after the index
        Permata before = get(idx-1);
        Permata after = get(idx);

        if (idx == size - 1) {
            insert(color);
            return;
        }
        
        Permata p = new Permata(color);
        p.next = after;
        p.prev = before;
        before.next = after.prev = p;

        size++;
    }

    boolean isInChain(int idx) {
        Permata pivot = get(idx);
        rightCount = 0;
        leftCount = 0;

        //Count nodes that are identical to the right
        //System.out.println("Index: " + idx); 
        Permata right = pivot.next;
        while (right.color == pivot.color && right.next != pivot) {
            //System.out.println("Stepping at rightCount: " + rightCount);
            rightCount++;
            right = right.next;
        } 
        //System.out.println("RC: " + rightCount);

        //System.out.println("!! RESET PIVOT !!");

        //Count idential nodes on right side
        Permata left = pivot.prev;
        while (left.color == pivot.color && left.prev != pivot) {
            //System.out.println("Stepping at leftCount: " + leftCount);
            leftCount++;
            left = left.prev;
        }
        //System.out.println("LC: " + leftCount);

        //Prepare deletion, find indexes between which the nodes are deleted
        chainStartIdx = idx - leftCount - 1;
        if (chainStartIdx < 0) 
            chainStartIdx += size;
        //System.out.println("Chain start after index: " + chainStartIdx);
        
        chainEndIdx = idx + rightCount + 1;
        if (chainEndIdx > size) 
                chainEndIdx -= size;
        //System.out.println("Chain end before index: " + chainEndIdx);

        chainSize = rightCount + leftCount + 1;

        /*System.out.println("Chain size: " + chainSize);
        if (chainSize >= 3) {
            System.out.println(idx + " is in a chain");
        }*/

        return chainSize >= 3;
    }

    void deleteChain(int idx) {
        //Deletes chain
        //System.out.println("Starting deletion at index: " + idx);

        if (chainSize >= size) { //CHeck if chain spans entire list
            head = null;
            size = 0;
            return;
        }

        Permata left = get(chainStartIdx);
        Permata right = get(chainEndIdx);

        left.next = right;
        right.prev = left;

        //Take care if old head gets deleted
        if(chainStartIdx > chainEndIdx) {
            head = right;
        }

        size -= chainSize;
        //System.out.println(size);

        //Set up combos
        int newIdx = idx - leftCount - 1; //Check the 'previous' node, not necessarily w/ index - 1
        if (newIdx < 0)
            newIdx += size;

        if (isInChain(newIdx))
            deleteChain(newIdx);
        
        //print(); //DEBUG
    }

    public void print() {
        Permata cur = head;

        do {
            System.out.print(cur.color + " ");
            cur = cur.next;
        } while (cur != head);

        System.out.println();
    }
}

public class SDA18192T {
    static Kalung game;
    static int maxSize;

    static void init() {
        //Loop through the necklace
        int counter = 0;
        do {
            checkGameStatus();
            if(game.isInChain(counter))
                game.deleteChain(counter);

            counter++;
        } while (counter < game.size);

        /*for (int i = 0; i < game.size; i++) {
            //System.out.println("Count: " + i);
            if (game.isInChain(i)) {
                //System.out.println(i + " is in a chain");
                game.deleteChain(i);
                if (game.size == 0) {
                    System.out.println("MENANG");
                    System.exit(0);
                }
            }
        }
        checkGameStatus(); */
    }

    static void checkGameStatus() {
        if (game.size > maxSize) {
            System.out.println("KALAH");
            System.exit(0);
        }
        if (game.size < 4) {
            System.out.println("MENANG");
            System.exit(0);
        }
    }

    static void tester() {
       Kalung k = new Kalung();
       
        //Tester function for Kalung operations
        System.out.println("========================");
        System.out.println("   Phase 1: Insertion   ");
        System.out.println("========================");

       //Insert
       System.out.println("1a) Insert at end");
       for (int i = 0; i < 5; i++) {
           k.insert(i+1);
       }
       k.print();

       //Insert at
       System.out.println("1b) Insert at index (here 7)");
       k.insertAt(2, 7);
       k.print();

       //Insert first
       System.out.println("1c) Insert first");
       k.insertFirst(9);
       k.print();

       System.out.println("========================");
       System.out.println("   Phase 2: Chains     ");
       System.out.println("========================");

       k = new Kalung();
       
       System.out.println("2a) Setup");
       for (int i = 0; i < 5; i++) {
           k.insert(i+1);
       }
       k.print();

       System.out.println("2b) Chain in middle");
       for (int i = 0; i < 3; i++) {
           k.insertAt(2, 3);
       }
       k.print();

       boolean inChain = k.isInChain(3);
       if (inChain) {
           System.out.println("Index 3 is in a chain, trying deletion");
           k.deleteChain(3);;
       }
       k.print();

       System.out.println("2c) Chain from start");
       for (int i = 0; i < 3; i++) {
           k.insertFirst(1);
       }
       k.print();

       inChain = k.isInChain(0);
       if (inChain) {
           System.out.println("Head is in a chain, trying deletion");
           k.deleteChain(0);
       }
       k.print();

       System.out.println("2d) Chain going through start");
       for (int i = 0; i < 3; i++) {
           k.insert(2);
       }
       k.print();

       inChain = k.isInChain(k.size - 1);
       if (inChain) {
           //System.out.println("Chain goes through start, trying deletion");
           k.deleteChain(k.size - 1);
       }
       k.print();

       //System.out.println("2e) Chain spans whole necklace");
       k = new Kalung();
       for (int i = 0; i < 3; i++) {
           k.insert(7);
       }
       k.print();

       inChain = k.isInChain(0);
       if (inChain) {
           k.deleteChain(0);
           System.out.println("Success! We have an empty chain!");
       }
    }
    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        try {
            String[] temp = br.readLine().split(" ");
            int startSize = Integer.parseInt(temp[0]);
            maxSize = Integer.parseInt(temp[1]);

            temp = br.readLine().split(" ");

            game = new Kalung();
            for (int i = 0; i < startSize; i++) {
                int color = Integer.parseInt(temp[i]);
                game.insert(color);
                //game.print(); //debug
            } 

            init();
            //game.print(); //debug

            //Main game loop
            int insertions = Integer.parseInt(br.readLine());

            for (int i = 0; i < insertions; i++) {
                temp = br.readLine().split(" ");
                int idx = Integer.parseInt(temp[0]);
                int colorVal = Integer.parseInt(temp[1]);
                
                if (idx == 0) {
                    game.insertFirst(colorVal);
                } else {
                    game.insertAt(idx, colorVal);
                }
                checkGameStatus();

                while (game.isInChain(idx)) {
                    game.deleteChain(idx);
                    checkGameStatus();    
                } 
                //game.print(); //DEBUG 
            }

            //Insertions ran out
            System.out.println(game.size);
            System.exit(0);

            //tester(); //DEBUG

        } catch (Exception exc) {
            System.out.println(exc);
        }
    }
}