import java.util.*;
import java.io.*;

public class Solusi {
    public static void main(String[] args) throws IOException {
        HashMap<String ,KelasPriorityQueue> mapKelas = new HashMap<String, KelasPriorityQueue>();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        //Menerima input kelas
        int n = Integer.parseInt(in.readLine());

        for(int i = 0; i < n; i++){
            String[] input = in.readLine().split(" ");
            String nama = input[0];
            
            int kuota = Integer.parseInt(input[1]);
            
            KelasPriorityQueue kelas = new KelasPriorityQueue(nama, kuota);
            mapKelas.put(nama, kelas);
        }
        
        //Menerima input mahasiswa
        int m = Integer.parseInt(in.readLine());
        for(int i = 0; i < m; i++){
            String[] input = in.readLine().split(" ");
            String[] inputKelas = input[2].split(",");
            
            ArrayList<KelasPriorityQueue> daftarKelas = new ArrayList<KelasPriorityQueue>();
            
            for(String kelas : inputKelas){
                daftarKelas.add(mapKelas.get(kelas));
            }
            
            Mahasiswa mahasiswa = new Mahasiswa(input[0],
            Integer.parseInt(input[1]), daftarKelas);

            for(int j = 0; j < daftarKelas.size(); j++){
                KelasPriorityQueue kelas = daftarKelas.get(j);
                kelas.add(mahasiswa);
        }
    }
    //Print mahasiswa disetiap kelas
    for (Map.Entry<String, KelasPriorityQueue> entry : mapKelas.entrySet()) {
            String key = entry.getKey();
            KelasPriorityQueue value = entry.getValue();
            System.out.println(key + " :");
            int index = 1;
            while(!value.isEmpty()){
                Mahasiswa mahasiswa = value.poll();
                System.out.println((index++) + ". " + mahasiswa.nama);
            }
        }
    }
}

class KelasPriorityQueue {
    Mahasiswa[] daftarMahasiswa;
    String nama;
    int kuota;
    int size;

    public KelasPriorityQueue(String nama, int kuota){
        this.nama = nama;
        this.kuota = kuota;
        this.size = 0;
        this.daftarMahasiswa = new Mahasiswa[kuota];
    }

    public boolean isEmpty(){
        return size == 0;
    }

    public int parentOf(int i) { return (i - 1) / 2; }

    public int leftChildOf(int i) { return 2 * i + 1; }

    public int rightChildOf(int i) { return 2 * (i + 1); }

    //HELPER FUNCTION UNTUK MENUKAR 2 MAHASISWA DALAM 2 POSISI BERBEDA
    public void swap(int m1, int m2) {
        //System.out.println("Swap " + m1 + " " + m2); //DEBUG
        Mahasiswa temp = daftarMahasiswa[m1];
        daftarMahasiswa[m1] = daftarMahasiswa[m2];
        daftarMahasiswa[m2] = temp;
    }

    public void add(Mahasiswa m){
        //System.out.println("Adding"); // DEBUG
        if (this.size == this.kuota) {
            //System.out.println("Overflow"); //DEBUG
            Mahasiswa root = daftarMahasiswa[0];

            //COMPARE SKILL
            if (root.getSkill() == m.getSkill()) {
                //System.out.println("Same skill"); //DEBUG
                //IF SAME, COMPARE NAME
                if (m.getNama().compareTo(root.getNama()) < 0) {
                    //System.out.println("Lexi earlier"); //DEBUG
                    daftarMahasiswa[0] = m;
                    percolateDown(0);

                    //KICK ROOT TO NEXT
                    KelasPriorityQueue next = root.daftarKelas.get(root.daftarKelas.indexOf(this) + 1);
                    next.add(root);
                }
            }
        } else {
            //System.out.println("Add last");
            daftarMahasiswa[this.size] = m;
            percolateUp(this.size);
            this.size++;
        }
    }

    public void percolateUp(int pos){
        //System.out.println("Perco up " + pos); //DEBUG
        //BASED OF SDA SLIDE
        int parent = parentOf(pos);

        while (pos > 0 && daftarMahasiswa[pos].getSkill() < daftarMahasiswa[parent].getSkill()) {
            //System.out.println("Pos " + pos); //DEBUG
            swap(parent, pos);
            pos = parent;
            parent = parentOf(pos);
        }
    }

    public void percolateDown(int pos){
        //System.out.println("Perco down " + pos); //DEBUG
       //BASED OF SDA SLIDE
       while (leftChildOf(pos) <= this.size) {
           //System.out.println("Pos " + pos); //DEBUG
           int childPos = leftChildOf(pos);
           //System.out.println("Child pos " + pos);
            if (rightChildOf(pos) < this.size && !daftarMahasiswa[childPos].isSmaller(daftarMahasiswa[childPos + 1])) {
                childPos++;
                //System.out.println("Child pos now " + pos);
            }

            if (!daftarMahasiswa[pos].isSmaller(daftarMahasiswa[childPos]))
                swap(pos, childPos);

            pos = leftChildOf(pos);            
        }
    }

    public Mahasiswa peek() {
        return daftarMahasiswa[0];
    }

    public Mahasiswa poll() {
        //System.out.println("Polling");
        Mahasiswa root = peek();
        daftarMahasiswa[0] = daftarMahasiswa[size - 1];
        percolateDown(0);
        return root;
    }
       

}
class Mahasiswa {
    String nama;
    int skill;
    ArrayList<KelasPriorityQueue> daftarKelas;

    public Mahasiswa(String nama, int skill, ArrayList<KelasPriorityQueue> daftarKelas){
        this.nama = nama;
        this.skill = skill;
        this.daftarKelas = daftarKelas;
    }

    public String getNama() { return this.nama; }
    public int getSkill() { return this.skill; }
    public ArrayList<KelasPriorityQueue> getDaftarKelas() { return this.daftarKelas; }

    //Helper function for lexicographical comparison
    public int compareTo(Mahasiswa m2) {
        return this.getNama().compareTo(m2.getNama());
    }

    public boolean isSmaller(Mahasiswa other) {
        return this.getSkill() < other.getSkill();
    }
}