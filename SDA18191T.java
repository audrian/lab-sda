import java.io.*;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;

/***
 * TP 1 SDA - 2018/2019 Semester I
    Muhammad Audrian - 1706043790
 */

public class SDA18191T {
    
    final static int MODULO = 1000000007;
    static int distance; //Jarak Dilan-Milea
    static int mileaSpeed; //Kecepatan Milea

    static int[] vehiclesSpeed; //Kecepatan semua kendaraan
    static int[] results; //Hasil semua fungsi A
    static int[] resultsB; //Hasil semua fungsi B
    static int[] optimum; //transport yang terakhir untuk F(s)

    //Explanation credits to Muhammad Indra Ramadhan & Dhipta Raditya
    //Menghitung berapa cara Dilan dam Milea dapat bertemu
    static int solverA(int s) {
        int result = 0;

        if (s < 0) result = -1;
        if (s == 0) result = 1;
        if (results[s] != -1) return results[s];
        
        for (int i = 0; i < vehiclesSpeed.length; i++) {
            if ((s - vehiclesSpeed[i] - mileaSpeed) >= 0) {
                /***DEBUG -------------------------------------------------------------------------------------
                System.out.println("Calculating F(" + Integer.toString(s-vehiclesSpeed[i]-mileaSpeed) + ")");
                //DEBUG ------------------------------------------------------------------------------------- */
                if ((results[s - vehiclesSpeed[i] - mileaSpeed]) != -1) { //Sudah diisi
                    //System.out.println(results[s - vehiclesSpeed[i] - mileaSpeed]); //DEBUG
                    result += results[s - vehiclesSpeed[i] - mileaSpeed] % MODULO;
                } else {
                    result += solverA(s - vehiclesSpeed[i] - mileaSpeed) % MODULO;
                }
            }
        }

        if (s < 0) System.out.println("NA");
        
        result = result % MODULO;
        results[s] = result;
        return result;
    }

    //Explanation credits to Muhammad Indra Ramadhan and Dhipta Raditya
    //Menghitung langkah minimal agar Dilan dan Milea bertemu
    static int solverB(int s) {        
        int result = 3002;

        //Base case
        if (s < 0) return 3002;
        if (s == 0) return 0;
        if (resultsB[s] != -1) return resultsB[s];
        
        //Recursion case
        for (int i = 0; i < vehiclesSpeed.length; i++) {
            //System.out.println(i); //DEBUG
            if (s - vehiclesSpeed[i] - mileaSpeed >= 0) {
                //System.out.println("Current: " + Integer.toString(s - vehiclesSpeed[i] - mileaSpeed)); //DEBUG
                if (solverB(s - vehiclesSpeed[i] - mileaSpeed) + 1 < result) {
                    optimum[s] = vehiclesSpeed[i];
                    result = solverB(s - vehiclesSpeed[i] - mileaSpeed) + 1;                
                }
            }
        }

        resultsB[s] = result;
        
        return result; //Jumlah langkah minimal
    }

    //Credits to Sage Muhammad Abdullah for the explanation
    static String printOptimum(int s) {
        String output = new String();
        if (solverB(s) == 3002) output = "NA";
            else {
                int temp = s;
                while (temp > 0) {
                    if (optimum[temp] != 0) {
                        output = output + (optimum[temp]) + " ";
                        temp = temp - optimum[temp] - mileaSpeed;
                    }
                } 
        }
        return output;
    }

    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        try {
            //Tentukan variabel-variabel
            String[] temp = br.readLine().split(" ");

            distance = Integer.parseInt(temp[0]);
            int noOfVehicles = Integer.parseInt(temp[1]);
            mileaSpeed = Integer.parseInt(temp[2]);

            vehiclesSpeed = new int[noOfVehicles];
            results = new int[distance + 1];
            resultsB = new int[distance + 1];
            optimum = new int[distance + 1];

            //DEBUG -------------------------------------------------------------
            //System.out.println(distance + " " + noOfVehicles + " " + mileaSpeed);
            //DEBUG -------------------------------------------------------------

            for (int i = 0; i < results.length; i++) {
                results[i] = -1;
                resultsB[i] = -1;
            }

            //DEBUG
            //System.out.println(Arrays.toString(results));

            String[] speeds = br.readLine().split(" ");

            /**DEBUG ---------------------------------
            for (int i = 0; i < speeds.length; i++) {
                System.out.println(speeds[i]);
            }
            //END DEBUG ----------------------------- */
            
            for (int i = 0; i < vehiclesSpeed.length; i++) {
                vehiclesSpeed[i] = Integer.parseInt(speeds[i]);
            }

            //Tipe soal
            String type = br.readLine();
            //System.out.println(type); //DEBUG
            
            //Solver
            if (type.equals("A")) {
                System.out.println(solverA(distance));
            } else if (type.equals("B")) {
                System.out.println(solverB(distance));
                /* DEBUG --------------------------------------
                for (int i = 0; i < resultsB.length; i++) {
                    System.out.println(i + ": " + resultsB[i]);
                } -------------------------------------------- */
                System.out.println(printOptimum(distance));

            } else {
                throw new IOException("Valid problem types are A and B");
            }
            
        } catch (Exception exc) {
            System.out.println(exc);
        }
    }
    
}