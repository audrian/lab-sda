import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

class RentangData implements Comparable<RentangData>{
	int l, r;
	long v;

	RentangData(int l, int r, long v) {
		this.l = l;
		this.r = r;
		this.v = v;
	}

	@Override
	public int compareTo(RentangData x) {
		if (v == x.v) {
			return l - x.l;
		} else {
			return (x.v - v) > 0 ? 1 : -1;
		}
	}
}

public class Rentang {

    ArrayList<RentangData> topRentang = new ArrayList<RentangData>();

    Rentang() {
        try {
            BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in));
            String par[] = br.readLine().split(" ");
            int N = Integer.parseInt(par[0]);  //Banyak data yang akan diproses
            int K = Integer.parseInt(par[1]);  //Banyaknya rentang terbesar bukan 0 yang dicari
            
            //Inisialisasi 
            int max = Integer.MIN_VALUE;
            int sum = 0;
            int left = 0;
            int right = 0;
            int temp = 0;

            //Cari dan baca data
            for(int i = 0; i < N; i++) {
                int x = Integer.parseInt(br.readLine());
                sum += x;

                if (max < sum) {
                    max = sum;
                    left = temp;
                    right = i;
                }

                if (sum <= 0) {
                    topRentang.add(new RentangData(left, right, max));
                    sum = 0;
                    temp = i+1;
                }
            }
            
            //Cetak K terbesar
            Collections.sort(topRentang);

           System.out.println(left + " " + right + " " + max);

        }
        catch(Exception e) { System.out.println(e); }
    }
    public static void main(String[] args) {
        new Rentang();
    }
}