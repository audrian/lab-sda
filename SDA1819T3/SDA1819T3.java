import java.util.Arrays;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.List;
/**
 * TP3 SDA - Muhammad Audrian 1706043790
 * Acknowledgements: Michael Christopher Sudirman (template), Visualgo, Geeks4Geeks, 
 * MightyPork (Tree Printer), Ariq Naufal Satria, Dhipta Raditya, Reynaldo Wijaya H, Gagah Pangeran R,
 * Giovan Isa Musthofa, Ramawajdi K Anwar, M. Farras Hakim, Slide SDA
 */

public class SDA1819T3 {
    static void tester() {
        AVLTree tree = new AVLTree();
        tree.root = tree.gabung("C");
        tree.root = tree.gabung("B");
        tree.root = tree.gabung("A");
        tree.root = tree.gabung("D");
        tree.root = tree.gabung("E");
        tree.root = tree.gabung("F");
        System.out.println(tree.distance("F"));
        System.out.println(tree.distance("A"));
        System.out.println(tree.lowestCommonAncestor("A", "F").data);
        System.out.println(tree.panjangTali("A", "F"));
        tree.musibah(tree.root);
        System.out.println();

        Node b = tree.get("B");
        Node e = tree.get("E");
        Node d = tree.get("D");
    }
    public static void main (String[] args){
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            AVLTree tree = new AVLTree();
            int count = Integer.parseInt(reader.readLine());
            //System.out.println(count);

            for (int i = 0; i < count; i++) {
                //System.out.println(i); //DEBUG
                String[] input = reader.readLine().split(" ");
                if (input[0].equalsIgnoreCase("GABUNG")) {
                    if (input.length > 2) {
                        ArrayList<Node> treeList = tree.treeToList();
                        String data = input[1];
                        int distance = Integer.parseInt(input[2]);

                        int rootIdx = treeList.indexOf(tree.root);

                        int index = 0;
                        for (int j = 0; i < treeList.size(); i++) {
                            if (data.compareTo(treeList.get(j).data) < 0) break;
                        }

                        if (Math.abs(rootIdx - index) <= distance) {
                            tree.root = tree.gabung(data);
                        }
                    } else {
                        tree.root = tree.gabung(input[1]);
                    }
                } else if (input[0].equalsIgnoreCase("PARTNER")) {
                    System.out.println(tree.partner(input[1]));
                } else if (input[0].equalsIgnoreCase("MENYERAH")) {
                    tree.root = tree.menyerah(input[1]);
                } else if (input[0].equalsIgnoreCase("CEDERA")) {
                    tree.root = tree.cedera(input[1]);
                } else if (input[0].equalsIgnoreCase("PRINT")) {
                    tree.print(tree.root);
                } else if (input[0].equalsIgnoreCase("MUSIBAH")) {
                    tree.root = tree.musibah(tree.root);
                    System.out.println();
                } else if (input[0].equalsIgnoreCase("PANJANG-TALI")) {
                    System.out.println(tree.panjangTali(input[1], input[2]));
                } else if (input[0].equalsIgnoreCase("GANTUNGAN")) {
                    ArrayList<String> nodes = new ArrayList<>();
                    
                    for (int j = 2; j < input.length; i++) {
                        nodes.add(input[j]);
                    }

                    System.out.println(tree.gantungan(nodes).data);
                }
            }
           //tester();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


class Node {
    Node left;
    Node right;
    String data;
    int height;
    public Node(String data){
        this.data = data;
        this.height = 1;
    }
    //Data compareTo
    int compareTo(Node compared){
        if (this.data.compareTo(compared.data) == 0) return 0;
        else if (this.data.compareTo(compared.data) > 0) return 1;
        else return -1;
    }

    public Node getLeft() { return this.left; }

    public Node getRight() { return this.right; }

    public String getText() { return this.data; }
}

class AVLTree {
    //Standard AVL Tree stuff
    Node root;
    
    public AVLTree() {
        this.root = null;
    }

    public AVLTree(Node root) {
        this.root = root;
    }
    //End stuff

    //HELPER TO CALCULATE HEIGHT OF A GIVEN NODE
    int height(Node node) {
        if (node == null) return 0;
        //System.out.println(node.data + ": Determining height");
        return node.height;
    }
    
    int distance(String data) {
        return distance(this.root, data);
    }

    //HELPER TO CALCULATE DISTANCE BETWEEN ROOT AND NODE
    int distance(Node root, String node) { //Credit Geeks for Geeks
        if (root == null) return 0;

       if (node.compareTo(root.data) < 0) return 1 + distance(root.left, node);
       if (node.compareTo(root.data) > 0) return 1 + distance(root.right, node);
       return 0;
    }

    //HELPER TO CALCULATE BALANCE FACTOR
    int balanceFactor(Node node) {
        if (node == null) return 0;
        return height(node.left) - height(node.right);
    }

    int depth(String name) {
        return depth(this.root, name);
    }
    
    int depth(Node root, String name) {
        if (name.compareTo(root.data) > 0) { //GO RIGHT
            return 1 + depth(root.right, name);
        } else if (name.compareTo(root.data) < 0) { //GO LEFT
            return 1 + depth(root.left, name);
        } else return 0; //THIS IS THE NODE WE'RE LOOKING FOR
    }

    ArrayList<Node> treeToList() {
        ArrayList<Node> result = new ArrayList<>();
        ttlHelper(result, this.root);
        return result;
    }

    void ttlHelper(ArrayList<Node> list, Node root) {
        if (root == null) return;

        ttlHelper(list, root.left);
        list.add(root);
        ttlHelper(list, root.right);
    }

    Node get(String name) {
        return get(this.root, name);
    }

    Node get(Node root, String name) {
        if (root == null) return null;

        if (name.compareTo(root.data) > 0) { //GO RIGHT
            return get(root.right, name);
        } else if (name.compareTo(root.data) < 0) { //GO LEFT
            return get(root.left, name);
        } else return root; //THIS IS THE NODE WE'RE LOOKING FOR
    }

    Node lowestCommonAncestor(String one, String two) {
        return lowestCommonAncestor(this.root, one, two);
    }

    //HELPER TO FIND LOWEST COMMON ANCESTOR
    Node lowestCommonAncestor(Node root, String one, String two) { //Credit Dhipta Raditya
        if (root == null) return null;
        if (one.compareTo(root.data) < 0 && two.compareTo(root.data) > 0) {
            //System.out.println("ke kanan");
            return lowestCommonAncestor(root.left, one, two);
        } else if (one.compareTo(root.data) > 0 && one.compareTo(root.data) > 0) {
            //System.out.println("ke kiri");
            return lowestCommonAncestor(root.right, one, two);
        } else {
            //System.out.println("dapet");
            return root;
        }
    }

    Node findMax(Node root) {
        return (root.right != null) ? findMax(root.right) : root;
    }

    Node findMin(Node root) {
        return (root.left != null) ? findMin(root.left) : root;
    }

    Node rotateRight(Node node) { //Credits to Reynaldo H Wijaya
        if (node == null) return null;

        Node pivot = node.left;
        node.left = pivot.right;
        pivot.right = node;

        node.height = Math.max(height(node.left), height(node.right)) + 1;
        pivot.height = Math.max(height(pivot.left), height(pivot.right)) + 1;

        return pivot;
    }

    Node rotateLeft(Node node) { //Credits to Reynaldo H Wijaya
        if (node == null) return null;

        Node pivot = node.right;
        node.right = pivot.left;
        pivot.left = node;

        node.height = Math.max(height(node.left), height(node.right)) + 1;
        pivot.height = Math.max(height(pivot.left), height(pivot.right)) + 1;

        return pivot;
    }

    Node rebalance(Node root) {
        //System.out.println("Rebalance " + root.data);
        //System.out.println("Balance factor " + balanceFactor(root));

        root.height = Math.max(height(root.left), height(root.right)) + 1;

        if (balanceFactor(root) == -2) {
            if (balanceFactor(root.right) > 0) root.right = rotateRight(root.right);
            return rotateLeft(root);
        }

        if (balanceFactor(root) == 2) {
            if (balanceFactor(root.left) < 0) root.left = rotateLeft(root.left);
            return rotateRight(root);
        }

        return root;
    }

    //ACTUAL FUNCTION THAT'S CALLED
    Node gabung(String data) {
        return gabung(this.root, data);
    }

    //HELPER FUNCTION - STANDARD AVL INSERT
    Node gabung(Node root, String data) {
        if (root == null) {
            //System.out.println("OK stop");
            root = new Node(data);
            return root;
        }
        // System.out.println(root.data);
        if (data.compareTo(root.data) < 0) {
            //System.out.println("taro kiri");
            root.left = gabung(root.left, data);
        }
        if (data.compareTo(root.data) > 0) {
            //System.out.println("taro kanan");
            root.right = gabung(root.right, data);
        }

        //System.out.println("itung tinggi");
        root.height = Math.max(height(root.left), height(root.right)) + 1;

        return rebalance(root);
        //System.out.println(root.height);
    }

    //returns the data inside node of either the left or the right
    //If exist right or left return the data inside node in right or left.
    String partner(String query) {
        return partner(this.root, query);
    }

    String partner(Node root, String query) { //Credits to GPR
        if (root == null) return "TIDAK ADA";

        if (root.left != null) {
            if (root.left.data.equals(query)) //Query is the left child
                return root.right.data;
        } else {
            if (root.right != null) {
                if (root.right.data.equals(query)) //Query is the right child
                    return root.left.data;
            }
        }

        if (query.compareTo(root.data) < 0) 
            return partner(root.left, query);

        if (query.compareTo(root.data) > 0)
            return partner(root.right, query);

        return "TIDAK ADA";
    }

    //Handles two cases of the Standard AVL Tree delete
    Node menyerah(String quitter){
        return delete(this.root, quitter, true);
    }

    //Handles two cases of the Standard AVL Tree delete
    Node cedera(String wounded){
        return delete(this.root, wounded, false);
    }

    //Modified Standard AVL Tree delete
    //with custom helper functions menyerah() and delete(), each handling two separate cases
    Node delete(Node root, String deleted, boolean quitting) {
        //HANDLE BASE CASE
        if (root == null) return null;

        if (deleted.compareTo(root.data) < 0) { //GO LEFT
            //System.out.println(deleted + ": " + root.data + ": Going left");
            root.left = delete(root.left, deleted, quitting);
        } else if (deleted.compareTo(root.data) > 0) { //GO RIGHT
            //System.out.println(deleted + ": " + root.data + ": Going right");
            root.right = delete(root.right, deleted, quitting);
        } else { 
            //System.out.println("found " + root.data);
            if (quitting) {
                //System.out.println("quitting");
                if (root.left != null && root.right != null) {
                    Node temp = findMax(root.left);
                    root.data = temp.data;
                    root.left = delete(root.left, temp.data, true);
                } else if (root.left != null || root.right != null) {
                    Node temp = (root.left != null) ? root.left : root.right;
                    root.data = temp.data;
                    if (root.left != null) root.left = null;
                    else root.right = null;
                } else {
                    root = null;
                }
            } else {
                //System.out.println("injured");
                if (root.left != null && root.right != null) {
                    Node pre = findMax(root.left);
                    Node suc = findMin(root.right);

                    Node temp = null;
                    if (height(pre) < height(suc)) {
                        temp = pre;
                    } else {
                        temp = suc;
                    }
                    root.data = temp.data;
                    if (temp.data == pre.data) root.left = delete(root.left, temp.data, false);
                    else root.right = delete(root.right, temp.data, false);
                } else if (root.left != null || root.right != null) {
                    Node temp = (root.left != null) ? root.left : root.right;
                    root.data = temp.data;
                    if (root.left != null) root.left = null;
                    else root.right = null;
                } else {
                    root = null;
                }
            }
        }
        if (root == null) return root;

        return rebalance(root);
    }

    //Prints the tree in pre order then post order
    void print(Node root){
        if (root == null) return;
        printPreOrder(root);
        System.out.println();
        printPostOrder(root);
        System.out.println();
    }

    //Prints the AVL Tree in pre order;
    //Root, left, right
    void printPreOrder(Node root){
        printHelper(root);
        if (root.left != null) printPreOrder(root.left);
        if (root.right != null) printPreOrder(root.right);
    }

    //Prints the AVL Tree in post order;
    //Left, Right, Root
    void printPostOrder(Node root){
        if (root.left != null) printPostOrder(root.left);
        if (root.right != null) printPostOrder(root.right);
        printHelper(root);
    }

    void printHelper(Node root) {
        if (root == null) return;
        System.out.print(root.data + ";");
    }

    //Prints the deleted tree in order
    //Left, root, right

    //Finds every leaf node, adds it to a string then deletes it.
    Node musibah(Node root) {
        //System.out.println(root.data);
        if (root == null) return null;

        if (root.left == null && root.right == null) {
            System.out.print(root.data + ";");
            return null;
        }

        if (root.left != null) root.left = musibah(root.left);
        //System.out.println(root.left);
        if (root.right != null) root.right = musibah(root.right);
        //System.out.println(root.right);

        return rebalance(root);
    }

    //Find distance between two nodes of a binary tree
    int panjangTali(String one, String two) {
        Node lca = lowestCommonAncestor(this.root, one, two);
        return distance(lca, one) + distance(lca, two);
    }

    //Lowest common ancestor for all given nodes in a list.
    Node gantungan(ArrayList<String> nodes) {
        //System.out.println(nodes.get(0));
        //System.out.println(nodes.get(1));
        Node lca = lowestCommonAncestor(this.root, nodes.get(0), nodes.get(1));
        //System.out.println(lca);

        if (nodes.size() > 2) {
            for (int i = 2; i < nodes.size() - 2; i++) {
                lca = lowestCommonAncestor(this.root, lca.data, nodes.get(i));
            }
        }

        return lca;
    }
}