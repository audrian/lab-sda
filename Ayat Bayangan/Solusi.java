import java.util.*;
import java.io.*;
class Node implements Comparable<Node>{
    public String namaKartu;
    public Node nodeKiri;
    public Node nodeKanan;
    public int leftSize;
    public boolean enabled;
    
	public Node(String namaKartu){
        this.namaKartu = namaKartu;
        this.nodeKiri = null;
        this.nodeKanan = null;
        this.enabled = true;
    }
    
	@Override
    public int compareTo(Node otherNode) {
        return this.namaKartu.compareTo(otherNode.namaKartu);
    }
}

class BinarySearchTree{
    public Node root;
    public Node resultNode;
    public String resultString;
    
	BinarySearchTree()
    {
        this.root = null;
        this.resultNode = null;
        this.resultString = null;
    }
    
	void insert(Node key){
        this.root = insert(this.root, key);
    }
    
	Node insert(Node root, Node key){
        //System.out.println(key.namaKartu); //DEBUG
        if (root == null) {
            root = key;
            return root;
        } else if (key.compareTo(root) > 0) {
            //System.out.println("less than " + root.namaKartu); //DEBUG
            root.nodeKanan = insert(root.nodeKanan, key);
        } else if (key.compareTo(root) < 0) {
            //System.out.println("more than " + root.namaKartu); //DEBUG
            root.leftSize++;
            root.nodeKiri = insert(root.nodeKiri, key);
        }

        return root;
    }
    
	int mainKartu(String target){
        return mainKartu(this.root, target);
	}
    
	int mainKartu(Node root, String target){
        //TODO
        if (root == null) {
            return 0;
        }

        if (root.namaKartu.compareTo(target) < 0) {
            return 1 + root.leftSize + mainKartu(root.nodeKanan, target);
        } else if (root.namaKartu.compareTo(target) > 0) {
            return mainKartu(root.nodeKiri, target);        
        } else {
            return mainKartu(root.nodeKanan, target);
        }

        /*if (target.compareTo(root.namaKartu) > 0) {
            //System.out.println("More"); //DEBUG
            ++count;
        }

        if (root.nodeKiri != null) {
            //System.out.println("Traverse left"); //DEBUG
            count += mainKartu(root.nodeKiri, target);
        }
        
        if (root.nodeKanan != null && root.nodeKanan.enabled) {
            //System.out.println("Traverse right");
            count += mainKartu(root.nodeKanan, target);
        }*/
    }
    
	void buangKartu(String rangeAwal, String rangeAkhir){
        buangKartu(this.root, rangeAwal, rangeAkhir);
    }
    
	void buangKartu(Node root, String targetAwal, String targetAkhir){
        //Credits to M. Yusuf Harahap for the algorithm
        int start = root.namaKartu.compareTo(targetAwal);
        int end = root.namaKartu.compareTo(targetAkhir);

        if (root == null) {
            return;
        } else {
            if (start <= 0 && end >= 0) {
                root.enabled = false;
            }

            if (start < 0) {
                buangKartu(root.nodeKiri, targetAwal, targetAkhir);
            }

            if (end > 0) {
                buangKartu(root.nodeKanan, targetAwal, targetAkhir);
            }
        }
	}
}

public class Solusi{
    public static void main(String[] args) throws Exception{
        BufferedReader br = new BufferedReader(new
		InputStreamReader(System.in));
        String input = br.readLine();
        String[] data = input.split(" ");
        int jumlahKartu = Integer.parseInt(data[0]);
        int jumlahQuery = Integer.parseInt(data[1]);
        int score = 0;
        
		BinarySearchTree tree = new BinarySearchTree();
		
        for(int I=0; I < jumlahKartu; I++){
            input = br.readLine();
            Node node = new Node(input);
            //System.out.println("Kartu " + node.namaKartu); //DEBUG
            tree.insert(node);
        }
        
		for(int I=0; I < jumlahQuery; I++){
            input = br.readLine();
            data = input.split(" ");
            if(data[0].equals("MAIN")){
                int hasil = tree.mainKartu(data[1]);
                score += hasil;
            }
            else{
                //tree.buangKartu(data[1], data[2]); //TODO IMPLEMENT
            }
        }
        
		System.out.println(score);
    }
}